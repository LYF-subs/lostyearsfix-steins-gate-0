# README #

# LostYearsFix: Steins;Gate 0

LostYearsFix is an amalgamation of "LostYears" and "HorribleFix" subtitles for Steins;Gate 0 to create the best version of subtitles for one of the most popular anime. 

## What was wrong with those subtitles to begin with?

Both had their own problems. LostYears made no attempt to fix basic issues about the translation:

- "Ruka" instead of "Luka" (which is the official translation the original authors specified)
- "Okabe Rintarou" instead of "Okabe Rintaro"
- "Kiryuu Moeka" instead of "Kiryu Moeka"
- "El Psy Congroo" instead of "El Psy Kongroo" (why do you think the mail address is "sg-epk" ?)
- "Radio Building" instead of "Radio Kaikan" (???)
- "Stein's Gate" lul
- multiple issues with song translations

On the other hand, HorribleFix was simulsubbed with the live release of 0, and instead had to rely on CR to translate items like building names, or quickly add them in themselves. LostYears instead brings seamless translations to almost every major sign and non-spoken text in Steins;Gate 0.

With these two combined, you now have the most cohesive version of subtitles for Steins;Gate 0, now with 100% more translated tutturu!

![](https://i.imgur.com/dI80GoV.png)

---

![](https://i.imgur.com/qx1uXEb.png)

![](https://i.imgur.com/b8RN3XX.png)

![](https://i.imgur.com/5uabsPN.png)

You can download the subtitles over on the [Downloads](https://bitbucket.org/LYF-subs/lostyearsfix-steins-gate-0/downloads/) tab. Make sure to install all fonts before using.

## changelog:

8/1/2021: Fixed karaoke translations to match ones by Rigs for episodes 12, 18, and 22. Also fixed an incorrectly translated Tutturu in episode 14.

7/30/2021: Inital release